from typing import (
    Dict,
    List
)
from string import punctuation


def check_row(
        row: str,
        frequency_dict: Dict[str, int]
) -> None:
    for mark in punctuation + '\n':
        row = row.replace(mark, '')

    for word in row.split(' '):
        word = word.strip().lower()
        frequency_count = frequency_dict.setdefault(word, 0) + 1
        frequency_dict[word] = frequency_count


def word_frequency(paragraph: List[str]) -> Dict[str, int]:
    paragraph_frequency_dict = {}

    for row in paragraph:
        check_row(
            row=row, frequency_dict=paragraph_frequency_dict
        )

    return paragraph_frequency_dict


if __name__ == "__main__":

    paragraph_ = [
        "The quick brown fox",
        "jumps over the lazy dog.",
        "The dog barks,",
        "and the fox runs away."
    ]
    frequency = word_frequency(paragraph=paragraph_)

    print(frequency)
